title: "pinephone (pro) modem disappearing with biktor's firmware"
date: 2022-05-23
---

When using [biktor's alternative modem firmware](https://github.com/Biktorgj/pinephone_modem_sdk),
you may find the modem disappearing after waking up from suspend.

To get rid of this problem persistently, the udev rules from eg25-manager need
to be adjusted as noted in
[SETTINGS.md](https://github.com/Biktorgj/pinephone_modem_sdk/blob/kirkstone/docs/SETTINGS.md).
If you run into this problem, it's recommended to adjust the udev rules
manually for now as described there.

Besides that, you can bring back the modem once by restarting eg25-manager:

```
$ sudo service eg25-manager restart
```

Adjusting the udev rules upstream is being discussed in
[eg25-manager!49](https://gitlab.com/mobian1/devices/eg25-manager/-/merge_requests/49).

UPDATE 2022-06-17: fixed in
[pma!3235](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3235),
[pma!3240](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3240)
