title: "Phosh top bar is borked with notch phones and custom margins"
date: 2023-07-07
---
The recent upgrade to Phosh 0.29.0 introduced a layout manager which takes
notches into account. This supersedes the workaround you may have made in 
pmOS-Tweaks if you have a phone with notches and wanted the clock to be visible
like on the OnePlus 6T. The custom margins may now move to battery indicator
into the location of the notch as seen below. To fix this either set the margins
in pmOS-Tweaks to `0`, or remove the lines in `~/.config/gtk-3.0/gtk.css` beginning
with `/* TWEAKS-START xxxx */` and ending with `/* TWEAKS-END xxxx */`.

[![The phosh's clock in the top bar was moved to the right but the battery indicator was moved in the middle of the top bar instead of immediately right of the clock.](/static/img/2023-07/phosh-top-bar-borked.png)](/static/img/2023-07/phosh-top-bar-borked.png)
