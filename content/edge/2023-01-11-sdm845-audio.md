title: "SDM845 no audio on Linux 6.1.3"
date: 2023-01-11
---

The Linux 6.1.3 stable upgrade caused a regression breaking audio on SDM845
devices.

The fix can be seen
[here](https://gitlab.com/sdm845-mainline/linux/-/commit/a5312556efefb2d93385681687a54fc5f6862c32)
and the MR to apply it is
[here](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3800)

The bug is due to an upstream fix enforcing that all apr sub-nodes in devicetree
have the "qcom,protection-domain" property. We have additional apr service nodes
for call audio support which were missing this property.

The patch introducing the regression has already been reverted upstream.
