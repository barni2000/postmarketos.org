title: "pinephone: suspend is broken for some users"
date: 2022-04-06
---

Since upgrading from 5.16.4 to 5.17.x, suspend is broken for some users. This
is still being investigated. Not everybody can reproduce this, getting a useful
trace of this bug is appreciated.

See [pma#1478](https://gitlab.com/postmarketOS/pmaports/-/issues/1478) for
details.

UPDATE: the kernel has been downgraded to 5.16.4 for now. Using `apk upgrade -a`
should get you the downgrade once the binary package has been built.
