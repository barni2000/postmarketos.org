title: "boot-deploy unable to find error when updating"
date: 2023-08-24
---

Some folks may hit an error in mkinitfs when updating that the dtb for their
device cannot be found. This is caused by a shell bug in boot-deploy.

This issue affects Android devices which boot using an Android boot image, and potentially other devicetree-based devices.

This is fixed in boot-deploy 0.10.2 which can be found in [this mr](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4358).

To fix, either wait until the new package is available before updating, or
install it now [with mrtest](https://wiki.postmarketos.org/wiki/Mrtest).

If your device is unbootable, you'll need to either build a new boot.img or
download the latest from the downloads page for your device. Once booted again,
ensure your device is up to date and then run `sudo mkinitfs` to regenerate the
boot image.
