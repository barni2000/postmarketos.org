title: "Crashes in GTK applications on the PinePhone"
date: 2023-01-24
---

After running for some time, some GTK applications print
`DRM_IOCTL_MODE_CREATE_DUMB failed: Out of memory` and then segfault.

This issue is being investigated in
[pmaports#1909](https://gitlab.com/postmarketOS/pmaports/-/issues/1909).
