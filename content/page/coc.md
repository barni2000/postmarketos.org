title: "Code of Conduct"
---
The [Alpine Linux Code of Conduct](https://www.alpinelinux.org/community/code-of-conduct.html)
is followed in postmarketOS.

Alpine is the distribution we are based on, and
our communities work closely together. The CoC is reasonable, it boils down to
using your brain and being nice to each other. It is what we have been living
since the project was started, for example people who were rude towards others
have been kicked from chats.

It helps to have this written down in detail, and when interacting with Alpine
you should be aware of this anyway. So it made sense to adopt this CoC for
postmarketOS.
