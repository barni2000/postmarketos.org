title: "v22.12 SP1: The One With A Photo Of A Librem 5 Taking A Photo Of A Librem 5 Taking A Photo"
title-short: "v22.12 SP1"
date: 2023-02-26
preview: "2023-02/sp1.jpg"
---

[![A Librem 5 running postmarketOS photographing a melting snow chicken/monster thing](/static/img/2023-02/sp1.jpg){: class="wfull border" }](/static/img/2023-02/sp1.jpg)

With the first service pack for v22.12 you can now reliably take photos of that
melting snow chicken/monster in your garden with a Librem 5! Thanks to new
Phosh related upgrades, it's also possible to use a bunch of cool widgets on
the lockscreen. Enable them in Phosh Mobile Settings, lock your phone and swipe
to the left. And there it is, your calendar or emergency contact information or
whatever you have configured! And there's more, find the detailed changelog
below.

## Contents

* New device:
    * [Samsung Galaxy Grand Max](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_Grand_Max_(samsung-grandmax))

* Phosh:
    * Upgrade Phosh from 0.22.0 to [0.24.0](https://gitlab.gnome.org/World/Phosh/phosh/-/releases/v0.24.0)
    * Upgrade Phoc from 0.21.1 to [0.24.0](https://gitlab.gnome.org/World/Phosh/phoc/-/releases/v0.24.0)
    * Upgrade phosh-mobile-settings 0.21.1 to [0.24.1](https://gitlab.gnome.org/guidog/phosh-mobile-settings/-/releases/v0.24.1)
    * Upgrade feedbackd 0.0.1 to [0.0.3](https://source.puri.sm/Librem5/feedbackd/-/releases/v0.0.3)
    * Upgrade feedbackd-device-themes 0_git20220202 to [0.0.3](https://source.puri.sm/Librem5/feedbackd-device-themes/-/releases/v0.0.3)
        * OnePlus 6: fix no ringtone on incoming call, no sound on SMS ([#1928](https://gitlab.com/postmarketOS/pmaports/-/issues/1928))

* Plasma Mobile:
	* Upgrade Plasma from 5.26.4 to [5.26.5](https://kde.org/announcements/plasma/5/5.26.5/)

* Purism Librem5:
    * Upgrade kernel from 6.1.1 to 6.2.0 ([!3896](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3896))
    * Fix unreliable back camera with millipixels ([#1947](https://gitlab.com/postmarketOS/pmaports/-/issues/1947))
    * Enable MGLRU ([!3850](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3850/))
    * Disable modem power management to make it more reliable ([!3902](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3902))

* PINE64 PinePhone & PineTab:
    * Upgrade kernel from 6.1.3 to 6.1.9 ([!3858](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3858))
    * Fix high CPU consumption ([#1901](https://gitlab.com/postmarketOS/pmaports/-/issues/1901))
        * Upgrade U-Boot from 2022.07 to 2023.01 ([!3806](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3806))
        * Upgrade arm-trusted-firmware from 2.7.0 to 2.8.0 ([!3806](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3806))
        * Apply this fix by manually
	  [upgrading u-boot](https://wiki.postmarketos.org/wiki/PinePhone_U-Boot_Upgrade)
	  or installing from a new image that has SP1 included
    * Enable DRAM frequency stats drivers and governors ([!3864](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3864))

* PINE64 PinePhonePro:
    * Upgrade kernel from 6.1.0 to 6.1.10 ([!3817](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3817))
    * Changed default CPU frequency governor to schedutil ([!3719](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3719))

* Replace postmarketos-base dependency on sudo with cmd:sudo ([!3721](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3721))

## How to get it

Find the most recent images at our [download page](/download/). Existing users
of the v22.12 release will receive this service pack automatically on their
next system update. If you read this blog post right after it was published, it
may take a bit until binary packages and new images are available.

Thanks to everybody who made this possible, especially our amazing community
members and upstream projects.
