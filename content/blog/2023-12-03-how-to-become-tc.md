title: "How to become a Trusted Contributor"
title-short: "Becoming a Trusted Contributor"
date: 2023-12-03
---
During the past team meetings, we have reworked the process for becoming a
Trusted Contributor (TC). To make it easier, more transparent, and to allow the
postmarketOS community to continue to grow as we review and integrate an
increasing amount of patches to improve stability, bring cool new features and
support more and more devices and usecases.

## What is a Trusted Contributor?

In postmarketOS, a Trusted Contributor is somebody who:

* ...has been supporting the project through positive contributions for a long
  time (could be own patches or just code review)
* ...has a good idea of how the project works, what our standards are regarding
  [Code Of Conduct](https://postmarketos.org/coc), for getting devices into
  various
  [categories](https://wiki.postmarketos.org/wiki/Device_categorization),
  [rules for merging](https://wiki.postmarketos.org/wiki/Rules_for_merging),
  etc.

They have the responsibilities and permissions to:

* Merge patches into most branches
* Do code reviews (a bit more officially than a regular community member, they
  can hit the approve button.)
* Close/move issues

Additional perks for TCs:

* A faster moving project! Our development workflow involves code review for
  most repositories (pmaports, etc.), so the more active TCs we have giving
  meaningful code reviews, the faster everyone's patches can get merged.
* Regular reviewers may prioritize each others patches as it makes sense.
* If desired, a possibility to become a Core Team member at some point.
  This is completely optional, as being a Core Team member means additional
  responsibilities (see below), and may as such not be desirable for every TC.
  We had people stepping down from the Core Team to becoming a TC in the past
  for being able to better focus on TC tasks / writing patches.
* Fame and glory of being a Trusted Contributor! In all seriousness, it is
  very useful to be able to point out just how much you have been contributing
  to free software projects in your free time, e.g. when applying for a job.

## Why we needed a new process

The previous process was not documented anywhere. But it was more or less: be
very active in the project for a long time until a Core Team member notices
your activity and asks you if you would like to become a Trusted Contributor.

This had several downsides:

* The time until a potential new TC gets noticed would not be consistent.
* Due to not being documented, it was not apparent at all how one could become
  a TC or that we even want people to get involved to the point that they could
  become new TCs.
* It is very likely that this lead to less people becoming a TC, which in turn
  means slower patch reviews and overall slower progress for postmarketOS.

## The new process

From now on we have this three step process for becoming a TC:

1. Be active in the community (postmarketOS, Alpine or related projects) for at
   least six months, doing all of these:

    * Submit your own patches, and take part in the code review process from
      patch author's side.
    * Do code reviews of other people's patches.
    * Help out in issues, community chats, etc.

2. Take note of the Core Team members and Trusted Contributors that you have
  interacted most with ([who?](https://wiki.postmarketos.org/wiki/Team_members)).
  You will need two people from TC or the Core Team to endorse you becoming TC.

3. Apply as Trusted Contributor by
   [creating a confidential issue](https://gitlab.com/postmarketOS/apply-for-trusted-contributor/-/issues/new).
   A [template](https://gitlab.com/postmarketOS/apply-for-trusted-contributor/-/blob/master/.gitlab/issue_templates/Default.md)
   asks you about your prior experience in postmarketOS etc.

## Who are these Core Team members you have been talking so much about?

In addition to TCs, the governance model of postmarketOS also has a Core Team.
They have the same duties as TCs, but additionally:

* Need to take part in (often very long)
  [meetings](https://wiki.postmarketos.org/wiki/Core_Team_Meetings)
  to discuss and steer the direction of the project
* Take care of behind-the-scenes organizational matters, maintain our
  infrastructure, etc.
* Create releases and service packs
* Make releases of pmOS projects (pmbootstrap, postmarketos-mkinitfs, ...)

We considered making a process for becoming a Core Team member as well. But
given that we currently have eight Core Team members and the overhead actually
increases to organize and do related team meetings with each new team member
(contrary to the code review process, where more TCs mean faster code
review), we do not plan to accept new team members at the moment. However if
somebody already is a TC and is convinced that it would be good for
postmarketOS if they became a Core Team member, and they would have the time
to actually do it: reach out to the Core Team via private message on matrix or
email, and we may consider it.

## Moving forward

So this is the process we came up with, and it may change as we refine and
learn from the results. We have a lot more exciting things planned, but let's
get the v23.12 release out first. Happy hacking!
