title: "v21.12 Service Pack 1"
title-short: "v21.12 SP1"
date: 2022-01-17
preview: "2022-01/ppkb.jpg"
---

[![](/static/img/2022-01/about-v21.12.1.jpg){: class="wfull border"}](/static/img/2022-01/about-v21.12.1.jpg)

Welcome to 2022! This is the first service pack of the year bringing some of the improvements from edge to stable. This
also brings a few nice new features to the v21.12 release that were not ready for inclusion yet when the last stable
release was made.

## Contents

* [Nokia N900](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2817) is back! It cheated death once again
  with the help of @sicelo and @danct12, who have picked up maintainership of the device, upgraded the kernel and done
  various fixes.

* [Allwinner Linux 5.15.3](https://github.com/megous/linux/releases/tag/orange-pi-5.15-20220105-2352) is now the latest
  kernel used for the PinePhone, this also brings support for the PINE64 hardware keyboard to stable, just in time
  since people have been receiving the units.

* [Sxmo 1.7.1](https://lists.sr.ht/~mil/sxmo-announce/%3C20220107164241.u7q5uaok5wkw37y6%40worker.anaproy.lxd%3E)
  is the latest and greatest Sxmo version, bringing visual voicemails among a lot of other things, see the
  neatly detailed release notes.

* [GNSS Share 0.4](https://gitlab.com/postmarketOS/gnss-share/-/tags/0.4)
  is now the GPS implementation for the Librem 5. This is a major component needed for getting assisted GPS working on
  this hardware improving the speed a GPS lock can be acquired.

* [Gnome Control Center](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2795) has a small bugfix that stops
  a crash when opening the printing panel so the printing system is now activated in Gnome. (Note that using the
  printing panel currently requires manually starting cups.)

[#grid side#]
[![](/static/img/2022-01/ppkb_thumb.jpg){: class="w300 border" }](/static/img/2022-01/ppkb.jpg)
[#grid text#]

## How to get it

Find the most recent images at our [download page](/download/). Existing users of the v21.12 release will receive this
service pack automatically on their next system update. If you read this blog post right after it was published, it may
take a bit until binary packages are available. Also for the N900 images, they will take a bit.

Thanks to everybody who made this possible, especially our amazing community members and upstream projects.
[#grid end#]

## Comments
* [Mastodon](https://fosstodon.org/web/@postmarketOS/107634826376664009)
* [Lemmy](https://lemmy.ml/post/151296)
<small>
* [HN](https://news.ycombinator.com/item?id=29961658)
</small>
