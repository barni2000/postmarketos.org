title: "v22.12 SP2: The One With The Nice Pull-Down Menu"
title-short: "v22.12 SP2"
date: 2023-04-10
preview: "2023-04/sp2.jpg"
---

[![A SHIFT6mq standing on grass. It runs postmarketOS v22.12 SP2 with Phosh 0.26.0 and shows the nice pull-down menu.](/static/img/2023-04/sp2.jpg){: class="wfull border" }](/static/img/2023-04/sp2.jpg)

Phosh upstream did this really nice Quick Settings menu style refresh that we
are happy to ship with this service pack, among other improvements. Without a
long introduction, enjoy the full list of changes:

## Contents

* Phosh:
    * Upgrade Phosh from 0.24.0 to [0.26.0](https://gitlab.gnome.org/World/Phosh/phosh/-/releases/v0.26.0)
    * Upgrade Phoc from 0.24.0 to [0.26.0](https://gitlab.gnome.org/World/Phosh/phoc/-/releases/v0.26.0)
    * Upgrade phosh-mobile-settings 0.24.1 to [0.26.0](https://gitlab.gnome.org/guidog/phosh-mobile-settings/-/releases/v0.26.0)

* Fix Waydroid: it was broken for several devices since the upstream image had
  been updated to LineageOS 18 (Android 11) and required new kernel options
  ([!3901](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3901),
   [!3975](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3975))

* Tuba:
    * [Tuba](https://tuba.geopjr.dev/) is a GTK4 Fediverse client, forked from
      the unmaintained [tootle](https://github.com/bleakgrey/tootle).
    * Add version [0.2.0](https://github.com/GeopJr/Tuba/releases/tag/v0.2.0)

* mobile-config-firefox: upgrade from 3.2.0 to [4.0.0](https://gitlab.com/postmarketOS/mobile-config-firefox/-/tags/4.0.0)

* Millipixels:
    * [Millipixels](https://source.puri.sm/Librem5/millipixels/) is a fork of
      the Megapixels camera application that works with the Librem 5.
    * Add version [0.21.0](https://source.puri.sm/Librem5/millipixels/-/commit/a18031a561b63775a63394f7d962e099f0b6a09b)
    * Upgrade dependency libcamera from 0.0.1 to [0.0.4](https://github.com/libcamera-org/libcamera/releases/tag/v0.0.4)

* Purism Librem 5
    * Upgrade kernel from 6.2.0 to 6.2.8
      ([!3986](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3986))
    * Upgrade librem5-base from 54 to
      [58](https://source.puri.sm/Librem5/librem5-base/-/blob/7e7ec167bdcb53c57c70776c0bddb9da58e99f0e/debian/changelog)
      ([!3989](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3989))

* PINE64 PinePhone Pro:
    * Upgrade kernel from 6.1.10 to 6.2.7
      ([!3967](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3967))
    * Enable Lazy RCU scheduler ([!3928](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3928))
    * Fix SD->eMMC installer prompt
      ([!3978](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3978),
       [related wiki notes](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_Pro_(pine64-pinephonepro)#Installing_postmarketOS))

* Samsung Galaxy S III:
    * Upgrade exynos4 kernel from 6.0.2 to 6.1.0
      ([!3717](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3717))

* Samsung Galaxy Tab 2 (7 inch, 10.1 inch):
    * Upgrade omap kernel from 6.1.0 to 6.2.1
      ([!3911](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3911))

* Xiaomi Mi Note 2:
    * Upgrade qcom-msm8996 kernel from 6.0.2 to 6.1.14
      ([!3910](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3910))

* PINE64 Pinebook Pro, RockPro 64:
    * Enable kexec
      ([!3999](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3999))

* PINE64 PinePhone, PineTab:
    * Enable FSCACHE and NFS caching
      ([!3979](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3979))

## Notes on SP1

As we found out later, the fix for high CPU consumption on the PinePhone
(original, not Pro) and PineTab mentioned in SP1 only works if postmarketOS is
installed to the SD card, not it if is installed to the eMMC. This is being
investigated in
[#1946](https://gitlab.com/postmarketOS/pmaports/-/issues/1946).
If you are affected, consider installing to SD card in the meantime or consider
contributing towards getting it fixed.

Careful readers will notice that it would have more sense to include
Millipixels in SP1 already, as we had backported related improvements in the
Librem 5 packaging. We missed it, but it is included here instead.

## How to get it

Find the most recent images at our [download page](/download/). Existing users
of the v22.12 release will receive this service pack automatically on their
next system update. If you read this blog post right after it was published, it
may take a bit until binary packages and new images are available.

Thanks to everybody who made this possible, especially our amazing community
members and upstream projects.
