title: "v22.06 SP2: The One That Swipes"
title-short: "v22.06 SP2"
date: 2022-09-04
preview: "2022-08/welcome_swipe.jpg"
---

[![](/static/img/2022-08/welcome_swipe.jpg){: class="wfull border" }](/static/img/2022-08/welcome_swipe.jpg)

Here it is, after a bit of delay to figure out why the new Phosh version didn't
boot on the Samsung Galaxy S III. Now that the reason is known and a workaround
is in place, we also happened to hit the timeframe where fixup versions of
these huge Phosh and Phoc releases were made. Enjoy the following changes on
stable!

## Contents

* [Phosh 0.21.0](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.21.0) and
  [Phoc 0.21.1](https://gitlab.gnome.org/World/Phosh/phoc/-/tags/v0.21.1) are
  huge upgrades from the previous versions 0.17.0 and 0.13.1. Both skipped a
  lot of version numbers to 0.20.0 in case you are wondering. Most excitingly
  it is now possible to swipe up and down to make menus show up. Also the
  lockscreen has been reworked, it's now possible to use the top menu there and
  for everyone who configured Phosh with pmOS tweaks to show the battery
  percentage, you will be able to see that right on the lockscreen too. See the
  changelogs of (Phosh) 
  [0.20.0](https://gitlab.gnome.org/World/Phosh/phosh/-/releases/v0.20.0) (and
  the 3 betas linked there),
  [0.21.0](https://gitlab.gnome.org/World/Phosh/phosh/-/releases/v0.21.0) and
  (Phoc)
  [0.20.0](https://gitlab.gnome.org/World/Phosh/phoc/-/tags/v0.20.0),
  [0.21.0](https://gitlab.gnome.org/World/Phosh/phoc/-/tags/v0.21.0),
  [0.21.1](https://gitlab.gnome.org/World/Phosh/phoc/-/tags/v0.21.1)
  for a detailed list of changes.

* [postmarketOS-welcome 0.6.0](https://gitlab.com/postmarketOS/postmarketos-welcome/-/tags/0.6.0)
  informs about how menus are now opened in Phosh (so users who did not read
  this blog post are not confused). Also the logic was changed to make it show
  up once when booting into this new Phosh version.

* [linux-postmarketos-qcom-sdm845 5.19.0](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3365)
  is a well-tested upgrade from edge for the SDM845 based SHIFT6mq, OnePlus
  6/6T and Xiaomi Pocophone F1.

* [powersupply 0.8.0](https://gitlab.com/MartijnBraam/powersupply/-/tags/0.8.0)
  (previously 0.6.0) is relevant for the SDM845 SoC as well, as it brings the
  necessary quirks to show the charging/discharging rate, temperature and USB current
  limits for SDM845 too.

* PinePhone Pro now has a workaround to fix audio if not having proper SMBIOS
  information coming from the bootloader
  ([pma#1601](https://gitlab.com/postmarketOS/pmaports/-/issues/1601)). This
  was properly fixed in Tow-Boot
  [2021.10-005](https://github.com/Tow-Boot/Tow-Boot/releases/tag/release-2021.10-005).

* PinePhone: kernel upgrade to
  [5.19.2](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3368) and
  the [AF8133J magnetometer](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3324)
  driver is now enabled

* PinePhone (Pro): add the upower fwupd plugin to prevent (modem firmware)
  upgrades if the phone is not connected to AC
  ([pma!3320](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3320))

* [Samsung Galaxy S III](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_S_III_(samsung-m0)):
  HDMI out had to be disabled as the kernel driver reports the wrong state when
  it is unplugged and that leads to the newer Phosh stack not starting up. This
  seems to be a rarely used feature as it requires an original dock/adapter
  from the time period. If you have one however and would like to fix this in
  the kernel, please
  [reach out](https://gitlab.gnome.org/World/Phosh/phosh/-/issues/828).

## How to get it

Find the most recent images at our [download page](/download/). Existing users of the v22.06 release will receive this
service pack automatically on their next system update. If you read this blog post right after it was published, it may
take a bit until binary packages and new images are available.

Thanks to everybody who made this possible, especially our amazing community members and upstream projects.
