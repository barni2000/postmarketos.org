# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
import collections
import flask
import markdown
import os
import re
import yaml

# same dir
import config
import page


def reading_time(content):
    content = re.sub('<[^<]+?>', '', content)
    words_per_minute = 200
    words = content.split(" ")
    ret = int(len(words) / words_per_minute)

    # Have at least one minute of reading time
    return ret if ret > 0 else 1


def parse_post(post, external_links=False, create_html=True,
               dir=config.content_dir_blog):
    """ :returns: a parsed post, something like this:
                  {"html": "<parsedhtmlcode...",
                   "url": "url/to/the/post",
                   # Path to an image inside static/img dir, to be displayed
                   # automatically when this post is posted on mastodon etc.
                   "preview": "2021-12/v21.12_pinebookpro.jpg",
                   "reading_time": "10 min",
                   "year": 2019} """
    data, content = page.parse_yaml_md(f"{dir}/{post}")
    y, m, d, slug = post[:-3].split('-', maxsplit=3)

    if create_html:
        data['html'] = markdown.markdown(content, extensions=[
            'markdown.extensions.extra',
            'markdown.extensions.codehilite',
            'markdown.extensions.toc'
        ], extension_configs={"markdown.extensions.toc": {"anchorlink": True}})
        data['html'] = page.replace(data['html'])

    func="blog_post" if dir == config.content_dir_blog else "edge_post"

    data['url'] = flask.url_for(func, y=y, m=m, d=d, slug=slug,
                                _external=external_links)
    data['reading_time'] = reading_time(content)
    data['year'] = y

    return data


def get_posts(dir=config.content_dir_blog, **kwargs):
    """ :returns: posts categorized by year, looks like:
                  {2019: [post1, post2, ...], 2018: [...], ...}
                  post1, post2 are the posts as returned by parse_post() above.
    """
    posts = sorted(os.listdir(dir), reverse=True)
    ret = collections.OrderedDict()
    for post in posts:
        parsed = parse_post(post, kwargs, dir=dir)
        year = parsed['year']
        if not year in ret:
            ret[year] = []
        ret[year].append(parsed)
    return ret

